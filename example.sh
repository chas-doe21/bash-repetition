#!/usr/bin/bash

echo "The script got the arguments: $@"

fun () {
    echo "Here are the arguments I was given: $@"
    echo "The first argument was: $1"
}

if [[ $0 = "./example.sh" ]]
then
    echo "ran example"
else
    echo "ran new example"
fi