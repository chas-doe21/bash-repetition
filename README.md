# Bash repetition

Today we'll go over some `bash`.

- [Bash repetition](#bash-repetition)
- [Sources](#sources)
- [Q&A](#qa)
- [Exercises](#exercises)
  - [Write a bash script](#write-a-bash-script)

# Sources

You can find the book we'll use today [here](https://tldp.org/guides.html).

# Q&A

Have you encountered something in Bash you don't understand/don't know how to implement/can't make work?

Ask away! :)

# Exercises

## Write a bash script

In [the book](./abs-guide.pdf) there are plenty of exercises. Choose one or more
and write a bash script.